package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	public Set<Property> list;
	
	
	public ComparePropertySessionBean() {
    	list = new HashSet<>();
    }
	
	@Override
	public void addProperty(Property property){
		list.add(property);
	};
	@Override
	public void removeProperty(Property property){
		for (Property p : list) {
			if (p.getPropertyId() == property.getPropertyId())
				list.remove(p);
			
		}
	};
	@Override
	public int bestPerRoom() {
		double bestPerRoom = 10000000;
		double roomPrice;
		Integer bestID=0;
		int numberRooms;
		for (Property p: list) {
			roomPrice = p.getPrice();
			numberRooms = p.getNumberOfBedrooms();
			
			if (roomPrice/numberRooms < bestPerRoom) {
				bestPerRoom = roomPrice/numberRooms;
				bestID = p.getPropertyId();
			}
			
			
		}
		return bestID;
		}
	
		
		
		
		
	@PostConstruct
	public void init() {
	    list=new HashSet<>();
	}	
	public CompareProperty create() throws CreateException, RemoteException {
	    return null;
	}
	public void ejbCreate() throws CreateException {
    }

}

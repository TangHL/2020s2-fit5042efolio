package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception {
    	//int id, String address, int numberOfBedrooms, int size, double price
        Property property1 = new Property(1,"24 Boston Ave, Malvern East VIC 3145, Asutralia",2,150,420000.00);
        Property property2 = new Property(2,"11 Bettina St, Clayton VIC 3168, Asutralia",3,352,360000.00);
        Property property3 = new Property(3,"3 Wattle Ave, Glen Huntly VIC 3163, Asutralia",5,800,650000.00);
        Property property4 = new Property(4,"3 Hamilton St, Bentleigh VIC 3204, Asutralia",2,170,435000.00);
        Property property5 = new Property(5,"82 Spring Rd, Hampton East VIC 3188, Asutralia",1,60,820000.00);
        
        propertyRepository.addProperty(property1);
        propertyRepository.addProperty(property2);
        propertyRepository.addProperty(property3);
        propertyRepository.addProperty(property4);
        propertyRepository.addProperty(property5);
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception {
    	List<Property> propertise = propertyRepository.getAllProperties();
    	System.out.println(propertise.size() + " properties added successfully");
    	for (Property elements : propertise) 
    	{
    		System.out.println(elements);
    	}
    	
    	
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception {
    	int id;
    	boolean flag = true;
    	Scanner console = new Scanner(System.in);
    	System.out.print("Enter the ID of the property you want to search: ");
    	id = console.nextInt();
    	//check id input 
    	while (flag) {
    		if (id < 1 || id > propertyRepository.getAllProperties().size()) {
    			System.out.print("please input a valid id number: ");
    			id = console.nextInt();
    		}else {
    			flag = false;
    		}
    	}
    	console.close();
    	System.out.println(propertyRepository.searchPropertyById(id));
        
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) throws Exception {
    	
    	
    	try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	private ArrayList<Property> properties;
	

    public SimplePropertyRepositoryImpl() {
    	properties = new ArrayList<>();
    }
    /**
     * Add the property being passed as parameter into the repository
     *
     * @param property - the property to add
     */
    public void addProperty(Property property) throws Exception {
    	
    	properties.add(property);
    };

    /**
     * Search for a property by its property ID
     *
     * @param id - the propertyId of the property to search for
     * @return the property found
     */
    public Property searchPropertyById(int id) throws Exception {
    	Property searchResult;
    	searchResult = properties.get(id-1);
    	return searchResult;
    };

    /**
     * Return all the properties in the repository
     *
     * @return all the properties in the repository
     */
    public List<Property> getAllProperties() throws Exception {
    	return properties;
    };
    
}